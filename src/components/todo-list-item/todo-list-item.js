import React, { Component } from 'react';
import './todo-list-item.css';

export default class TodoListItem extends Component {

    // constructor () {
    //     super();
    //     this.onLabelClick = () => {
    //         console.log(`Done ${this.props.label}`);
    //     }
    // }

    // state = {
    //     done: false,
    //     important: false
    // }

    // onLabelClick = () => {
    //     this.setState( ( { done } ) => {
    //         return {
    //             done: !done
    //         }
    //     });
    // };

    // onMarkImportant = () => {
    //     this.setState( ( { important } ) => {
    //         return {
    //             important: !important
    //         }
    //     });
    // }
    

    render () {
        const { label, onDeleted, onToggleImportant, onToggleDone, important, done } = this.props;


        let classNames = 'todo-list-item d-flex';

        if (done) {
            classNames += ' done';
        }

        if (important) {
            classNames += ' important';
        }

        return (
        <span className = {classNames}>
            <span className = 'todo-list-item-label'
              onClick = { onToggleDone }
            >
              { label }
            </span>
            <div className = 'd-flex'>
                <button type = 'button'
                        className = 'btn btn-outline-success btn-sm float-rigth'
                        onClick = { onToggleImportant }>
                    <i className="fa fa-exclamation" />
                </button>
                <button type = 'button'
                        className = 'btn btn-outline-danger btn-sm float-right'
                        onClick = { onDeleted }>
                    <i className="fa fa-trash-o" />
                </button>
            </div>
        </span>    
        
        );
    };
};




