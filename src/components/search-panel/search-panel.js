import React, { Component } from 'react';
import './search-panel.css';

export default class SearchPanel extends Component {

    state = {
        term: ''
    }

    onSearchChange = (e) => {
        e.preventDefault();
        const term = e.target.value;
        this.setState({
            term: term
        })
        this.props.onSearchChange(term)
    }

    render() {

    const searchText = 'Type here to search';
    const searchStyle = {
        fontSize : '18px'
        }
     return (
        <form>
             <input className = 'form-control search-panel'
                  type="search"
                  placeholder={ searchText } 
                  style={ searchStyle }
                  value = { this.state.term }
                  onChange = {this.onSearchChange} />
        </form>
           )
     }
};

